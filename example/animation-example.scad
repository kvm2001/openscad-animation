include <animation.scad>
$fn=20;

fps=25;
repeat_animation = 1;
print_info=false;

//scenario = [
//    zeroact("c", tr=[0,0,0.5]),
//    zeroact("v", v=0.1),
//    act("c", d=2, tr=[0, lin(10), twohalfsin(5)], r=lin(x=360)),
//    scene(d=1, acts = [
//        act("c", tr=lin(x=5), r=lin(y=360)),
//        act("v", v=lin(2)),
//    ]),
//    act("c", d=2, tr=twohalfsin(y=lin(-10), z=5), r=lin(z=360)),
//    scene(d=1, acts = [
//        act("c", tr=lin(x=-5), r=lin(y=-360)),
//        act("v", v=lin(-2)),
//    ]),
//    act("c", d=3, tr=[0,0, twohalfsin(5)])
//];

function scenario() = [
//    zeroact("c", tr=[0,0,0.5]),
//    zeroact("v", v=0.1),
//    act("c", d=2, tr=[0, lin(10), twohalfsin(5)], r=lin(x=360)),
//    scene(s=1*i(3), d=1, acts = [
//        act("c", tr=lin(x=5), r=lin(y=360)),
//        act("v", v=lin(2)),
//    ]),
    act("c", s=1+0.5*i("c"), d=2, tr=twohalfsin(y=lin(-10), z=5), r=lin(z=360)),
    act("", d=1)
];
//    actor == "i" ? i(tr=[-i*2,0,0]) :
//    actor == "o" ? i(tr=[0,0,0.001*i], r=[0, i * 90, 0]) :
//    0;

actor_count = [
    "c", 5,
    "a", 7
];

echo (count=get_actor_count("a"));

for (i=[0:5]) {
//    v=value("v", undef, i);
    translate([-2 * i,0,0]) {
        c(i);
        color("black") linear_extrude(0.01) projection() c(i);
    }
}
echo([undef,2]/3);
module c(i) {
    v=0.1;//value("v");
    translate([-2.5, 0, 0])
    name("c", i)
//    name("o", i)
    difference() {
        cube([1, v, 3], center=true);
        color("red") translate([0,0,-v/2 - 1.5]) cube(v + 2, center=true);
        color("lightgreen") translate([0,0,v/2 + 1.5]) cube(v + 2, center=true);
    }
}