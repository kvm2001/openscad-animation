scenario = [];
actor_count = [];
default_act_duration = 2;
fps = 25;
$get_max_i = false;

durations = get_acts_durations(scenario($get_max_i=true));
total_duration = durations[0] + durations[1] + durations[2];
total_frames = fps * total_duration;
current_time = total_duration * $t - durations[0];

//flatten_scenario = flatten_act_list(scenario);
//enriched_scenario = enrich_scenario(flatten_scenario);
object_properties = get_object_properties(scenario, current_time, durations[0]);
        
animate = true;
repeat_animation = 1;
override_video_files = true;
output_gif = false;
print_info = true;

print_info();

function init_animation(fps) =
    let($fps = fps)
        print_info();

module print_info() {
    if($t == 0 || print_info) {
        echo("=====|    Animation    |=====");
        echo(str("Total duration: ", total_duration, " sec"));
        if (fps == 25) {
            echo("FPS: 25 (default)");
        } else {
            echo(str("FPS: ", fps));
        }
        echo(str("Steps: ", total_frames));
        if (current_time != 0) {
            echo(str("Current time: ", current_time));
        }
        
        echo("Use this command to make a video:");
        ffmpeg_override = override_video_files ? "-y" : "";
        ffmpeg_command = output_gif
            ? str("ffmpeg ", ffmpeg_override,  " -framerate ", fps, " -i frame%05d.png -start_number 0 animation.gif")
            : str("ffmpeg ", ffmpeg_override,  " -framerate ", fps, " -i frame%05d.png -start_number 0 -c:v libx264 -pix_fmt yuv420p video.mp4");
        
        if (repeat_animation == 1 || output_gif) {
            echo(ffmpeg_command);
        } else {
            create_input_file_command = str("> input-list.txt && for i in {1..", repeat_animation, "}; do printf \"file '%s'\\n\" video.mp4 >> input-list.txt; done");
            ffmpeg_repeat_commad = str("ffmpeg ", ffmpeg_override,  " -f concat -i input-list.txt -c copy repeat.mp4");
            
            echo(str(ffmpeg_command, " && ", create_input_file_command, " && ", ffmpeg_repeat_commad));
        }
        echo("=====| end of Animation |=====");
    } else {
        echo(str("Steps: ", total_frames));
    }
}

// -----|   object   |-----
module name(object_name, i) {
//    names = is_list(object_name) ? object_name : [object_name];
    if (animate) {
        let(sc=scenario($current_i=i, $current_actor=object_name),
            val=get_object_properties(sc, current_time, durations[0]),
            properties = get_actor_properties(object_name, val),
            tr = properties[1][0],
            r = properties[1][1]) {
                if (object_name == "c") {
//            echo(name=val);
//            echo(sc=sc);
                }
//            echo(object_name=object_name, i=i, tr=tr, r=r, ith=ith("i", 1));
            translate(tr) rotate(r) children();
        }
    } else {
        children();
    }
}

function i(actor) =
    !is_string(actor) ? assert(false, "Function i() called without actor set") :
    $get_max_i ? get_actor_count(actor) - 1 :
    $current_i;

function get_actor_count(actor) =
     let(index = search([actor], actor_count),
        count = actor_count[index[0] + 1])
//    echo(actor_count=actor_count, index=index, count=count)
    is_num(count) ? count : 1;

function value(object_name, i) =
//    let(names = is_list(object_name) ? object_name : [object_name])
    animate
        ? let(val = get_object_properties(scenario($current_i=i, $current_actor=object_name), current_time, durations[0]),
            properties = get_actor_properties(object_name, val),
            v = first_defined(properties[1][2], 1))
//            echo(val=val)
            v
        : default_value;

// -----|          |-----
function sum_all_translations(names, i=0) = 
    let (tr = get_translate(names[i]))
    i == len(names) - 1 ? tr : tr + sum_all_translations(names, i+1);

function sum_all_rotations(names, i=0) = 
    let (r = get_rotation(names[i]))
    i == len(names) - 1 ? r : r + sum_all_rotations(names, i+1);

function sum_all_values(names, i=0) = 
    let (val = get_value(names[i]))
    i == len(names) - 1 ? val : val + sum_all_values(names, i+1);

function get_translate(object_name) = 
    let (props = track_object_properties(enriched_scenario, object_name),
         tr = props[3])
    is_undef(tr) || len(tr) == 0
        ? [0,0,0]
        : tr;

function get_rotation(object_name) = 
    let (props = track_object_properties(enriched_scenario, object_name),
         r = props[4])
    is_undef(r) || len(r) == 0
        ? [0,0,0]
        : r;

function get_value(object_name) = 
    let (props = track_object_properties(enriched_scenario, object_name))
         props[5];

function track_object_properties(scenario, object_name, start_from_act = 0) = 
    len(scenario) <= start_from_act ? [] :
    let(act = scenario[start_from_act],
        is_applicable = is_applicable_to(act, object_name),
        is_time = is_good_time(act, current_time),
        subsequent_acts = track_object_properties(scenario, object_name, start_from_act + 1))
    is_applicable && is_time
        ? merge_act(act, subsequent_acts)
        : subsequent_acts;

// -----|   New scenario processing   |-----
function get_object_properties(acts, current_time, current_scenario_time, current_act=0) =
    is_empty(acts) ? [] :
    let(item = acts[current_act],
        item_property_list = get_properties_of_item(item, current_time, current_scenario_time),
        durations = get_scenario_item_durations(item))
    is_last(current_act, acts) ? item_property_list :
    let(next_scenario_time = increment_time(current_scenario_time, durations),
        next_properties = get_object_properties(acts, current_time, next_scenario_time, current_act + 1))
    is_empty(item_property_list) && is_empty(next_properties) ? [] :
    let(merged_properties = merge_properties(item_property_list, next_properties))
    merged_properties;

// returns property list of scenario item applicable by time
function get_properties_of_item(item, current_time, current_scenario_time) =
    is_scene(item) ? get_scene_property_list(item, current_time, current_scenario_time) :
    is_actor(item) ? get_actor_property_list(item, current_time, current_scenario_time) :
    get_properties_of_act(item, current_time, current_scenario_time);

// returns property list of scene acts applicable by time
// tested
function get_scene_property_list(scene, current_time, current_scenario_time, item=0) =
    let(scene_start = first_defined(scene[0], 0),
        duration = scene[1],
        acts = scene[2],
        
        act = acts[item],
//        durations = get_raw_act_durations(act, duration, duration, true),
        act_duration = first_defined(get_act_duration(act), duration - first_defined(act[1], 0), default_act_duration),
        limited_duration = min(act_duration, duration),
        
        act_start = scene_start + first_defined(act[1], 0),
        act_duration = durations[1] + durations[2],
        overwritten_act = act(act[0], act_start, limited_duration, tr=act[3], r=act[4], v=act[5], async=true),
        act_start_time = current_scenario_time + first_defined(act[1], 0))
        
//    echo(overwritten_act=overwritten_act, current_time=current_time, current_scenario_time=current_scenario_time, durations=durations)
    is_empty(acts) ? [] :
    let(act_properties = get_properties_of_act(overwritten_act, current_time, current_scenario_time))
    is_last(item, acts) ? act_properties :
    merge_properties(act_properties, get_scene_property_list(scene, current_time, current_scenario_time, item + 1));

// returns property list of actor acts applicable by given time
// tested
function get_actor_property_list(actor_item, current_time, current_scenario_time, item=0) =
    let(actor = actor_item[0],
        acts = actor_item[1],
        act = acts[item])
    is_empty(acts) ? [] :
    let(default_actor = actor,
        act_properties = get_properties_of_act(act, current_time, current_scenario_time, default_actor))
    is_last(item, acts) ? act_properties :
    merge_properties(act_properties, get_actor_property_list(actor_item, current_time, current_scenario_time, item + 1));

// returns property list of act applicable by time.
// tested
function get_properties_of_act(act, current_time, act_start_time, default_actor) =
    is_empty(act) ? [] :
    let(act_start = act_start_time + first_defined(act[1], 0),
        act_duration = first_defined(get_act_duration(act), default_duration),
        ratio = get_act_progress_ratio(act_start, get_scenario_item_durations(act), act_duration, current_time),
        actor = first_defined(act[0], default_actor))
//    echo(act=act, current_time=current_time, act_start_time=act_start_time, ratio=ratio)
    [[actor, calculate_act_properties(act, ratio)]];

// calc act properties based on progress ratio and approximation functions
function calculate_act_properties(act, ratio) = 
    ratio == -1 ? [] :
    let(t = act[3],
        r = act[4],
        v = act[5],
        translate = calculate_property(t, ratio),
        rotate = calculate_property(r, ratio),
        value = calculate_property(v, ratio))
    [translate, rotate, value];

// calculate real value of properties to be applied based on act passed part ratio
// tested
function calculate_property(property, ratio) =
    let(plain_value = unqualify_value(property),
        qualifier = get_qualifier(property))
    ratio == 1 ? get_passed_value(qualifier, plain_value) :
    ratio == -1 ? [] :
    get_current_act_value(qualifier, ratio, plain_value);

// calculated what part of the given act duration has passed
// returns number from 0 to 1 or -1 if act hasn't started yet
// tested
function get_act_progress_ratio(scenario_time, act_durations, duration, current_time) = 
    let(act_start = scenario_time + act_durations[0],
        total_act_duration = duration)
//    echo(scenario_time=scenario_time, act_durations=act_durations, current_time=current_time, act_start=act_start, total_act_duration=total_act_duration)
    current_time < act_start ? -1 : // haven't started yet
    current_time >= act_start + total_act_duration || total_act_duration == 0  // completed or zeroact
        ? 1 :
    (current_time - act_start) / total_act_duration; // in progress

// increment current scene time by given durations
// tested
function increment_time(act_start_time, durations) = 
//    echo(durations=durations)
    act_start_time + durations[1];

// return list of properties of different actors
// earlier items in the list contains summed properties, later items - input properties
// adding inputs to output needed for cases when an actor contained only in a single input list
// tested
function merge_properties(property_list, next_property_list) = 
    is_empty(property_list) ? next_property_list :
    is_empty(next_property_list) ? property_list :
    let(summed_properties = summ_property_lists(property_list, next_property_list))
    len(property_list) == 1 && len(next_property_list) == 1
        && property_list[0][0] == next_property_list[0][0] ? summed_properties :
    len(property_list) == 1 && len(next_property_list) == 1 
        ? concat(property_list, next_property_list) :
    len(property_list) == 1
        ? concat(summed_properties, next_property_list) :
    let(all_plain_properties = concat(property_list, next_property_list)) // TODO concat only different actors
    concat(summed_properties, all_plain_properties);

// summs property lists for different actors
// returns list of result properties with actor names
// tested
function summ_property_lists(property_list, next_property_list, current_property = 0) =
    let(properties = property_list[current_property],
        actor = properties[0], // TODO merge only actors from both lists
        next_properties = get_actor_properties(actor, next_property_list),
        result_properties = summ_properties(properties, next_properties))
    is_last(current_property, property_list) ? [result_properties] :
    is_empty(next_properties) ? [result_properties] :
    let(next = summ_property_lists(property_list, next_property_list, current_property + 1))
    is_empty(result_properties) ? next :
    concat([result_properties], next);

// return summ of given two properties.
// tested
function summ_properties(properties, next_properties) =
    is_empty(properties) && is_empty(next_properties) ? [] :
    is_empty(properties) ? next_properties :
    is_empty(next_properties) ? properties :
    sum_lists(properties, next_properties);

// returns first properties found by actor name [actor_name, properties]
// tested
function get_actor_properties(actor, property_list) =
    let(index = search(actor, property_list),
        found = !is_empty(index))
    found ? property_list[index[0]] : [];

// returns first properties found by actor name [actor_name, properties]
// recursive alternative without warning if not found
// tested
function get_actor_properties(actor, property_list, i=0) =
    property_list[i][0] == actor ? property_list[i] :
    is_last(i, property_list) ? [actor, [[0,0,0],[0,0,0],0]] :
    get_actor_properties(actor, property_list, i + 1);

// -----|   scenario   |-----
function act(n=undef, s, d, tr=[0,0,0], r=[0,0,0], v=undef, async=false) = [n, s, d, tr, r, v, async];

function zeroact(n=undef, s, tr=[0,0,0], r=[0,0,0], v=undef) = [n, s, 0, tr, r, v, false];

function actor(actor_name, acts) = [actor_name, acts];

function scene(s, d, acts) = [s, d, acts];

function scenario() = [];

function get_act_start(act) = act[1];

function get_act_duration(act) = act[2];

function get_act_async(act) = act[6];

function is_async_act(act) = get_act_async(act) == true;

function get_act_end(act) = get_act_start(act) + get_act_duration(act);

function is_applicable_to(act, object_name) =
    is_empty(act) ? false : act[0] == object_name;

function is_good_time(act, current_time) = 
    get_act_start(act) <= current_time;

function is_time(act_start_time, current_time) = 
    act_start_time >= current_time;

function read_act(scenario, act_num, current_time) = 
    let(is_unknown_act = act_num == len(scenario),
        act = scenario[act_num],
        is_count_current_act = get_act_end(act) <= current_time)
    is_unknown_act
        ? []
        : is_count_current_act
            ? merge(act, read_act(scenario, act_num + 1))
            : read_act(scenario, act_num + 1);

function enrich_scenario(scenario, act_num=0, act_start=0, prev_act_duration=0) =
    is_empty(scenario) ? [] :
    let(act = scenario[act_num],
        is_last_act = act_num == len(scenario) -1,
        is_prev_act_async = act_num == 0 ? false : get_act_async(scenario[act_num - 1]),
        start_from_prev = is_prev_act_async ? act_start - prev_act_duration : act_start,

        raw_start = get_act_start(act),
        start = is_undef(raw_start) ? start_from_prev : raw_start,
        act_with_start = is_undef(raw_start) ? replace_element_with(act, start, 1) : act,

        raw_duration = get_act_duration(act),
        duration = is_undef(raw_duration) ? default_act_duration : raw_duration,
        act_with_duration = is_undef(raw_duration) ? replace_element_with(act_with_start, duration, 2) : act_with_start,
        
        act_to_use = act_with_duration)
    is_last_act ? [act_to_use] :
        concat([act_to_use], enrich_scenario(scenario, act_num + 1, start + duration, duration));

function flatten_act_list(scenario, index = 0) =
    is_empty(scenario) ? [] :
    let (act = scenario[index],
        require_flatten = need_to_flatten(act),
        to_return = require_flatten ? act : [act])
    index == len(scenario) - 1 ? to_return : concat(to_return, flatten_act_list(scenario, index + 1));

function need_to_flatten(act) = 
    // in act s is a number
    len(act) < 2 || is_list(act[1]) ? true : false;
    
// -----|   common list functions   |-----
function has_element (vector, el) = is_num(search([el], vector)[0]);

function concat_as_is (el1, el2) = is_undef(el2) ? [el1] : concat([el1], [el2]);

function list_of_size_with(len_sample, element, position) = 
    let(length = len(len_sample))
    length == 0 ? [] :
    length == 1 ? [element] :
    position == 0 ? concat(element, empty_list_of_size(length-1)) :
    position == length - 1 ? concat(empty_list_of_size(length-1), element) :
        concat(empty_list_of_size(position), concat(element, empty_list_of_size(length - position - 1)));

function sum_lists(list1, list2, from_index=0) =
    let(el1 = list1[from_index],
       el2 = list2[from_index],
       last_index = len(list1) - 1,
       invalid_index = from_index >= len(list1))
   invalid_index ? [] : // case when first list is empty
   let (el_sum = sum_elements(el1, el2))
   from_index == last_index ? [el_sum] : concat([el_sum], sum_lists(list1, list2, from_index +1));

function sum_elements(el1, el2) = 
    is_undef(el1) ? el2 :
    is_undef(el2) ? el1 :
    el2 == 0 ? el1 :
    is_string(el1) ? el1 :
    is_num(el1) && is_num(el2) ? el1 + el2 :
    is_bool(el1) && is_bool(el2) ? el1 || el2 :
    is_list(el1) && is_list(el2) ? sum_lists(el1, el2) :
    assert(false, str("Sum of elements ", el1, " and ", el2, " not supported yet"));

function sublist(list, from, length = undef) =
    is_empty(list) ? [] :
    from < 0 || from > len(list) - 1 ? [] :
    length == 0 ? [] :
    let(el = list[from],
        remain = is_undef(length) ? length : length - 1)
    remain == 0 || from == len(list) -1
        ? [el]
        : concat([el], sublist(list, from + 1, remain));

function replace_element_with(list, element, position) =
    is_empty(list) ? list :
    position < 0 || position >= len(list) ? list :
    position == 0 ? concat([element], sublist(list, 1)) :
    position == len(list) - 1 ? concat(sublist(list, 0, len(list) - 1), [element]) :
    concat(sublist(list, 0, position), concat([element], sublist(list, position + 1)));

function insert_element_at(list, element, position) =
    position < 0 || position > len(list) ? list :
    position == 0 ? concat([element], list) :
    position == len(list) ? concat(list, [element]) :
    concat(sublist(list, 0, position), concat([element], sublist(list, position)));

function is_empty(vector) =
    is_undef(vector) || !is_list(vector) || len(vector) == 0;

function empty_list_of_size(size) =
    size == 0 ? [] :
    size == 1 ? [0] :
    size == 2 ? [0, 0] :
    size == 3 ? [0, 0, 0] :
    size == 4 ? [0, 0, 0, 0] :
    size == 5 ? [0, 0, 0, 0, 0] :
    size == 6 ? [0, 0, 0, 0, 0, 0] :
    assert(false, str("Size ", size, " is not supported yet"));
    
function first_defined(p1, p2, p3, p4, p5) =
    !is_undef(p1) ? p1 :
    !is_undef(p2) ? p2 :
    !is_undef(p3) ? p3 :
    !is_undef(p4) ? p4 :
    !is_undef(p5) ? p5 :
    assert(false, "No param is defined. Please provide some default value");

function get_empty_value_of_type(sample) =
    is_undef(sample) ? undef :
    is_bool(sample) ? false :
    is_string(sample) ? "" :
    is_num(sample) ? 0 :
    empty_list_of_size(len(sample));

function is_last(index, list) =
    index == len(list) - 1;

function contains(list, element) =
    let(result = search(element, list),
        found = !is_empty(result))
    found;

// -----|   merge scenario into properties   |-----
function merge_act(act, current_state) =
    concat(act[0], merge_act_properties(act, current_state, 1));
    
function merge_act_properties(act, state, index) =
//    echo(act=act, state=state, index=index)
    let (raw_act_val = is_empty(act) 
            ? get_empty_value_of_type(state[index])
            : act[index],
         state_val = is_empty(state) 
            ? get_empty_value_of_type(unqualify_value(act[index]))
            : state[index],
         qualifier = get_qualifier(raw_act_val),
         act_val = eval_act_val(act, raw_act_val),
         val = act_val + state_val,
         size = is_empty(act) ? len(state) : len(act),
         is_last_val = index >= size -1)
//     echo(raw_act_val=raw_act_val, state_val=state_val, act_val=act_val, val=val)
    is_empty(act) && is_empty(state) ? [] :
    is_last_val
        ? [val]
        : let(next_val = merge_act_properties(act, state, index + 1))
            concat([val], next_val);

function eval_act_val(act, val) =
    is_empty(act) ? val :
    let(plain_value = unqualify_value(val),
        qualifier = get_qualifier(val))
    get_act_end(act) <= current_time ? get_passed_value(qualifier, plain_value) :
    get_act_start(act) > current_time ? assert(false, "Should be checked earlier") :
    let(ratio = (current_time - get_act_start(act)) / get_act_duration(act))
    get_current_act_value(qualifier, ratio, plain_value);

function unqualify_value(val) =
    is_empty(val) ? val :
    is_string(val[0]) ? val[1] : val;
     
function get_qualifier(val) =
    is_empty(val) ? "none" :
    is_string(val[0]) ? val[0] : "none";

// processes values like "linear", [1, ["sinus", 5], ["halfsin", 3]] when act has passed
function get_passed_value(default_qualifier, plain_value, i=0) =
//    echo(default_qualifier=default_qualifier, plain_value=plain_value, i=i)
    is_string(plain_value) ? plain_value :
    is_bool(plain_value) ? plain_value :
    is_num(plain_value) ? process_passed_function(default_qualifier, plain_value) :
    is_list(plain_value) ?
    is_empty(plain_value) ? [] :
    
    let(is_last = i == len(plain_value) - 1,
        raw_val = plain_value[i],
        val = unqualify_value(raw_val),
        val_qualifier = get_qualifier(raw_val),
        qualifier = val_qualifier == "none" ? default_qualifier : val_qualifier,
        result_value = process_passed_function(qualifier, val))
    is_last ? [result_value] :
        concat([result_value], get_passed_value(default_qualifier, plain_value, i + 1))
    : undef;

function process_passed_function(qualifier, plain_value) =
    qualifier == "fullsin" ? 0 : // fullsin returns object to its original state, so nothing is changed
    qualifier == "twohalfsin" ? 0 : // twohalfsin returns object to its original state, so nothing is changed
    plain_value;

// processes values like "linear", 0.123, [1, ["sinus", 5], ["halfsin", 3]] when act is in progress
function get_current_act_value(default_qualifier, ratio, plain_value, i=0) =
//    echo(default_qualifier=default_qualifier, ratio=ratio, plain_value=plain_value)
    is_string(plain_value) ? plain_value :
    is_bool(plain_value) ? plain_value :
    is_num(plain_value) ? process_ratio(default_qualifier, ratio) * plain_value :
    is_list(plain_value) ?
    is_empty(plain_value) ? [] :

//    echo(default_qualifier=default_qualifier, ratio=ratio, plain_value=plain_value)
    let(is_last = i == len(plain_value) - 1,
        raw_val = plain_value[i],
        val = unqualify_value(raw_val),
        val_qualifier = get_qualifier(raw_val),
        qualifier = val_qualifier == "none" ? default_qualifier : val_qualifier,
        result_value = process_ratio(qualifier, ratio) * val)
    is_last ? [result_value] :
        concat([result_value], get_current_act_value(default_qualifier, ratio, plain_value, i + 1))
    : undef;

function process_ratio(qualifier, ratio) =
    "none" == qualifier ? 1 :
    "linear" == qualifier ? ratio :
    "sinus" == qualifier ? sin(ratio * 180 - 90) / 2 + 0.5 :
    "fullsin" == qualifier ? sin(ratio * 360 - 90) / 2 + 0.5 :
    "halfsin" == qualifier ? sin(ratio * 90):
    "twohalfsin" == qualifier ? sin(ratio * 180) :
    "revhalfsin" == qualifier ? sin(ratio * 90 - 90) + 1:
    assert(false, str("Transition function not supported: ", qualifier));

// -----|   transition   |-----
function lin(single, x = 0, y = 0, z = 0) = ["linear", build_single_or_xyz(single, x, y, z)];
function sinus(single, x = 0, y = 0, z = 0) = ["sinus", build_single_or_xyz(single, x, y, z)];
function fullsin(single, x = 0, y = 0, z = 0) = ["fullsin", build_single_or_xyz(single, x, y, z)];
function halfsin(single, x = 0, y = 0, z = 0) = ["halfsin", build_single_or_xyz(single, x, y, z)];
function twohalfsin(single, x = 0, y = 0, z = 0) = ["twohalfsin", build_single_or_xyz(single, x, y, z)];
function revhalfsin(single, x = 0, y = 0, z = 0) = ["revhalfsin", build_single_or_xyz(single, x, y, z)];

function build_single_or_xyz(single, x, y, z) =
    is_undef(single) ? [x, y, z] : single;

// -----|   time   |-----
function get_total_duration(scenario) = 
    is_empty(scenario) ? 0 :
    get_max_act_end(scenario, 0);
    
function get_max_act_end(scenario, act_num) =
    let (act = scenario[act_num],
        is_last_act = act_num == len(scenario) - 1)
    is_last_act ? get_act_end(act)
        : max(get_act_end(act), get_max_act_end(scenario, act_num + 1));

function get_scenario_duration(scenario) = 
    let(durations = get_acts_durations(scenario))
    durations[0] + durations[1] + durations[2];

function is_scene(item) =
    len(item) == 3 && is_list(item[2]) && !is_list(item[1]) && !is_list(item[0]);

function is_actor(item) = 
    len(item) == 2 && is_list(item[1]) && !is_list(item[0]);

function is_act_list(item) =
    is_list(item[0]);

function get_scenario_item_durations(item, default_act_duration=default_act_duration, duration_limit, default_async) = 
    let(durations = 
        is_scene(item) ? get_scene_durations(item) :
        is_actor(item) ? get_actor_durations(item) :
//        is_act_list(item) ? get_acts_durations(item) :
        get_raw_act_durations(item, default_act_duration, duration_limit, default_async))
    durations;

function early_start(durations) =
    durations[0];

function sync_part(durations) =
    durations[1];

function async_part(durations) =
    durations[2];

function get_raw_act_durations(act, default_act_duration, duration_limit, default_async) = 
    let(start_delay = first_defined(get_act_start(act), 0),
        early_start = start_delay < 0 ? -start_delay : 0,
        
        duration = first_defined(get_act_duration(act), duration_limit - start_delay, default_act_duration),
        extended_duration = is_undef(duration_limit)
            ? max(0, duration + start_delay) : min(duration, duration_limit),
        
        is_async = is_async_act(act) || default_async,
        sync_duration = is_async ? 0 : extended_duration,
        async_duration = is_async ? extended_duration : 0, 
        result = [early_start, sync_duration, async_duration])
//    echo(act=act, default_act_duration=default_act_duration, duration_limit=duration_limit, start_delay=start_delay, early_start=early_start, duration=duration, extended_duration=extended_duration, is_async=is_async, sync_duration=sync_duration, async_duration=async_duration, result=result)
    result;

function get_actor_durations(actor, act_num=0) = 
    let(acts = actor[1])
    get_acts_durations(acts);

function get_scene_durations(scene, act_num=0) = 
    let(acts = scene[2],
        scene_duration = scene[1],
        acts_durations = get_acts_durations(acts, 0, scene_duration, scene_duration, default_async=true),
        scene_start = first_defined(scene[0], 0),
        scene_total_duration = scene_duration + scene_start,
        scene_early_start = max(-first_defined(scene_start, 0), acts_durations[0]),
        max_act_duration = acts_durations[2], // async part used because acts in scene are async
        async = acts_durations[2] > scene_duration ? acts_durations[2] - scene_duration : 0)
//    echo(acts_durations=acts_durations, act_d=acts_durations[1])
    [scene_early_start, scene_total_duration, async];

function get_acts_durations(acts, act_num=0, default_act_duration=default_act_duration, duration_limit, default_async) =
    is_empty(acts) ? [0,0,0] :
    let(act = acts[act_num],
        act_durations = get_scenario_item_durations(act, default_act_duration, duration_limit, default_async))
    is_last(act_num, acts) ? act_durations :
    
    let(later_durations = get_acts_durations(acts, act_num + 1, default_act_duration, duration_limit, default_async))
//    echo(act_durations, later_durations)
    merge_durations(act_durations, later_durations);

function merge_durations(earlier_durations, later_durations) = 
    let(
        remaining_later_early_start = max(0, early_start(later_durations) - sync_part(earlier_durations)),
        start_delay = max(early_start(earlier_durations), remaining_later_early_start),

        later_duration_increment = sync_part(later_durations),
        earlier_duration_increment = sync_part(earlier_durations),
        sync_duration = earlier_duration_increment + later_duration_increment,
        
        async_later_duration = async_part(later_durations),
        async_earlier_duration = max(0, async_part(earlier_durations) - later_duration_increment),
        async_tail = max(async_later_duration, async_earlier_duration),
        
        res = [start_delay, sync_duration, async_tail]
    )
//    echo(rl=remaining_later_early_start, sd=start_delay, ldi=later_duration_increment, adi=act_duration_increment, ald=async_later_duration, at=async_tail)
//    echo(ed=earlier_durations, ld=later_durations, res=res)
    res;
