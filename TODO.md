Todo:

1. Implement scenario transformation
1. Make all acts in a scene to be async, but total scene duration is what is set in scene itself.
Act start (if set) is calculated from scene start(as a sum).
If act has some duration set (override scene duration) and this duration is longer than scene duration, then remaining part becomes an async tail of the scene.
1. Zeroact sets values as absolute by default. v and d are also set as absolute by default. Other values in acts are set as relative (tr, r) by default. s is set as relative to previous act (implemented).
1. Add camera actor. Maybe add ability to set camera properties with other actors as well?
1. Add infinity duration
1. Add rpm() to scenario