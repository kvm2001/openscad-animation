# Warning
This library is a very young and has no stability in anything except the key concepts:

* Model and animation are **separate**
* Animation is **declarative**
* Animation scenario is **easy** to write, read and modify

Anything than that could be changed. But I believe that there will be a release or more and they will add some stability into the library usage.

# How to use
Save [lib/animation.scad](https://bitbucket.org/kvm2001/openscad-animation/src/master/lib/animation.scad) file to any of the options:

* (Simplest) any directory you're going to work
* (Better) some dedicated directory for OpenSCAD libraries
* (Even better) default OpenSCAD `lib` directory. Refer "Library Locations" from the [document](https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Libraries)
* (The best) checkout this repository using git to any directory you want and then create a link from the default OpenSCAD path (see above) to the library file. This way you can get updates easily.

Next include the library into your OpenSCAD file (you have to use `include` instead of `use`):

    include <animation.scad>

Then define some object and give it a name:

    name("c") cube(1);

After modeling (and naming) is done you can add some animation scenario (typically before the model itself):

    scenario = [
        act("c", s=0, d=1, tr=lin(x=5)),
        act("c", s=1, d=1, tr=lin(y=5)),
    ];
##### Short explanation: 
* *Scenario* consists of *acts*
* *Name* of the object is an *actor* in the *act* (first parameter)
* `s` stands for *Start time* (in seconds) from the *scenario* start (I'm going to change this soon)
* `d` stands for *duration* (in seconds)
* `tr` stands for *translation*. It accepts typical [x, y, z] vector. This vector is relative to the previous object position and is applied immediately as act starts
* `lin()` function adds time interpolation to the given value. That means, that returned vector become closer to the given value as time goes during this *act*.

After you render you scad file using `Preview` (`F5`) you'll see instruction in the OpenSCAD console about *FPS* and *Steps* you have to enter into the OpenSCAD animation panel. Follow these instructions and you'll see the animation.

# Example
You can find the simple example of the animation in the [animation-test.scad](https://bitbucket.org/kvm2001/openscad-animation/src/master/animation-test.scad) file.