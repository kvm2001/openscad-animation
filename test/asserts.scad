// asserts that first actor item from properties start is the same as expected for the actor
module assert_final_property(actual, expected) {
    for (i = [0 : len(expected)-1]) {
        expected_actor_prop = expected[i];
        actor = expected_actor_prop[0];
        
        found = search(actor, actual);
        if (len(found) == 0) {
            echo(str("Properties for actor ", actor, " is not found in ", actual));
            assert(false);
        }
        
        actual_actor_prop = actual[found[0]];
        if (actual_actor_prop != expected_actor_prop) {
            echo(str("Actual properties ", actual_actor_prop, " doesnt match expected ", expected_actor_prop));
            assert(false);
        }
    }
}

// asserts that both values are equal
module assert_equal(actual, expected) {
    if (actual != expected) {
        echo(str("Actual value ", actual, " doesnt match expected ", expected));
        assert(false);
    }
}
