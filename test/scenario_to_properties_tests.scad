include <animation.scad>
use <asserts.scad>

assert_equal(increment_time(5, [0,3,0]), 8);
assert_equal(increment_time(5, [0,0,3]), 5);
assert_equal(increment_time(5, [2,1,0]), 6);
assert_equal(increment_time(5, [2,0,1]), 5);
assert_equal(increment_time(5, [2,3,4]), 8);
assert_equal(increment_time(5, [0,3,4]), 8);
assert_equal(increment_time(5, [2,0,0]), 5);

assert_equal(get_act_progress_ratio(2, [0, 3, 0], 3, 0), -1);
assert_equal(get_act_progress_ratio(2, [0, 3, 0], 3, 2), 0);
assert_equal(get_act_progress_ratio(2, [0, 3, 0], 3, 3), 1/3);
assert_equal(get_act_progress_ratio(2, [0, 3, 0], 3, 4), 2/3);
assert_equal(get_act_progress_ratio(2, [0, 3, 0], 3, 5), 1);
assert_equal(get_act_progress_ratio(2, [0, 3, 0], 3, 6), 1); // add more for different start and async tail

assert_equal(calculate_property([1,2,3], 0.5), [1,2,3]); // none
assert_equal(calculate_property([1,2,3], 0), [1,2,3]); // none
assert_equal(calculate_property([1,2,3], 1), [1,2,3]); // none
assert_equal(calculate_property(lin([1,2,3]), 0.5), [0.5, 1, 1.5]);
assert_equal(calculate_property([lin([1,2,3]), lin([4,5,6]), lin(8)], 0.5), [[0.5, 1, 1.5], [2,2.5,3],4]);
assert_equal(calculate_property(lin([1,2,3]), 0), [0, 0, 0]);
assert_equal(calculate_property(lin([1,2,3]), 1), [1, 2, 3]);
assert_equal(calculate_property(lin([1,2,halfsin(3)]), 0.5), [0.5, 1, 3 * process_ratio("halfsin", 0.5)]);

assert_equal(get_properties_of_act(act("a", s=0, d=1, tr=[1,2,3], r=[4,5,6]), -1, 0), [["a", []]]);
assert_equal(get_properties_of_act(act("a", s=0, d=1, tr=[1,2,3], r=[4,5,6]), 0, 0), [["a", [[1,2,3],[4,5,6],undef]]]);
assert_equal(get_properties_of_act(act(undef, s=0, d=1, tr=[1,2,3], r=[4,5,6]), 0, 0, "a"), [["a", [[1,2,3],[4,5,6],undef]]]);

assert_equal(get_actor_properties("a", [["a", [1,2]]]), ["a", [1,2]]);
assert_equal(get_actor_properties("a", [["b", 3], ["a", [1,2]]]), ["a", [1,2]]);
assert_equal(get_actor_properties("a", [["a", 3], ["a", [1,2]]]), ["a", 3]);
assert_equal(get_actor_properties("a", [["b", [1,2]]]), ["a", [[0, 0, 0], [0, 0, 0], 0]]);

assert_equal(summ_properties(["a", 1, [2, 3]], ["a", 4, [5, 6]]), ["a", 5, [7, 9]]);
assert_equal(summ_properties([], ["a", 4, [5, 6]]), ["a", 4, [5, 6]]);
assert_equal(summ_properties(["a", 1, [2, 3]], []), ["a", 1, [2, 3]]);
assert_equal(summ_properties([], []), []);
assert_equal(summ_properties(["b", 1, [2, 3]], ["a", 4, [5, 6]]), ["b", 5, [7, 9]]);

assert_equal(summ_property_lists([["a", 1]], [["a", 2]]), [["a", 3]]);
assert_equal(summ_property_lists([["a", 1], ["b", 4]], [["a", 2], ["b", 5]]), [["a", 3], ["b", 9]]);

assert_equal(merge_properties([["a", 1]], [["a", 2]]), [["a", 3]]);
assert_equal(merge_properties([["a", 1], ["b", 4]], [["b", 5], ["a", 2]]),
    [["a", 3], ["b", 9], ["a", 1], ["b", 4], ["b", 5], ["a", 2]]);

assert_equal(get_actor_property_list(actor("a", [act(s=0, d=0, v=3)]), 0, 0),
    [["a", [[0, 0, 0], [0, 0, 0], 3]]]);
assert_equal(get_actor_property_list(actor("a", [act(s=0, d=3, v=lin(3))]), 0, 0),
    [["a", [[0, 0, 0], [0, 0, 0], 0]]]);
assert_equal(get_actor_property_list(actor("a", [act(s=0, d=2, v=lin(3))]), 1, 0),
    [["a", [[0, 0, 0], [0, 0, 0], 1.5]]]);
assert_equal(get_actor_property_list(actor("a", [act(s=0, d=2, v=lin(3))]), 3, 0),
    [["a", [[0, 0, 0], [0, 0, 0], 3]]]);
assert_equal(get_actor_property_list(actor("a", [act(s=0, d=2, v=3)]), 0, 1),
    [["a", []]]);

assert_equal(get_scene_property_list(scene(d=2, acts=[act("a", v=3)]), 0, 0),
    [["a", [[0, 0, 0], [0, 0, 0], 3]]]);
assert_equal(get_scene_property_list(scene(d=2, acts=[act("a", v=lin(3))]), 1, 0),
    [["a", [[0, 0, 0], [0, 0, 0], 1.5]]]);
assert_equal(get_scene_property_list(scene(d=2, acts=[act("a", v=lin(3))]), 3, 0),
    [["a", [[0, 0, 0], [0, 0, 0], 3]]]);

assert_final_property(get_object_properties([
    zeroact("c", tr=[0,0,0.5]),
    zeroact("v", v=0.1),
    act("c", d=2, tr=[0, lin(10), twohalfsin(5)], r=lin(x=360)),
], 0, 0), [
    ["c", [[0, 0, 0.5], [0, 0, 0], undef]],
    ["v", [[0, 0, 0], [0, 0, 0], 0.1]],
]);

assert_final_property(get_object_properties([
    zeroact("c", tr=[0,0,0.5]),
    zeroact("v", v=0.1),
    act("c", d=2, tr=[0, lin(10), twohalfsin(5)], r=lin(x=360)),
], 1, 0), [
    ["c", [[0, 5, 5.5], [180, 0, 0], undef]],
    ["v", [[0, 0, 0], [0, 0, 0], 0.1]],
]);

assert_final_property(get_object_properties([
    zeroact("c", tr=[0,0,0.5]),
    zeroact("v", v=0.1),
    act("c", d=2, tr=[0, lin(10), twohalfsin(5)], r=lin(x=360)),
    scene(d=2, acts = [
        act("c", tr=lin(x=5), r=lin(y=360)),
        act("v", s=1, v=lin(3)),
    ]),
], 3.5, 0), [
    ["c", [[3.75, 10, 0.5], [360, 270, 0], undef]],
    ["v", [[0, 0, 0], [0, 0, 0], 1.6]],
]);

assert_final_property(get_object_properties([
    zeroact("c", tr=[0,0,0.5]),
    zeroact("v", v=0.1),
    act("c", d=2, tr=[0, lin(10), twohalfsin(5)], r=lin(x=360)),
    scene(d=1, acts = [
        act("c", tr=lin(x=5), r=lin(y=360)),
        act("v", v=lin(3)),
        act("d", v=lin(3)),
    ]),
], 3, 0), [
    ["d", [[0, 0, 0], [0, 0, 0], 3]],
    ["c", [[5, 10, 0.5], [360, 360, 0], 0]],
    ["v", [[0, 0, 0], [0, 0, 0], 3.1]],
]);

assert_final_property(get_object_properties([
    zeroact("c", tr=[0,0,0.5]),
    zeroact("v", v=0.1),
    act("c", d=2, tr=[0, lin(10), twohalfsin(5)], r=lin(x=360)),
    actor("v", acts = [
        act(d=2, tr=lin(x=5), r=lin(y=360), async=true),
        act(s=1, d=2, v=lin(3)),
    ]),
], 3.5, 0), [
    ["c", [[0, 10, 0.5], [360, 0, 0], undef]],
    ["v", [[3.75, 0, 0], [0, 270, 0], 1.6]],
]);

assert_final_property(get_object_properties([
    act("c", s=1, d=2, tr=[0, lin(10), 0]),
], 1, 0), [
    ["c", [[0, 0, 0], [0, 0, 0], undef]],
]);

assert_final_property(get_object_properties([
    act("c", s=1, d=2, tr=[0, lin(10), 0]),
], 2, 0), [
    ["c", [[0, 5, 0], [0, 0, 0], undef]],
]);

//s=[
//    zeroact("c", tr=[0,0,0.5]),
//    zeroact("v", v=0.1),
//    act("c", d=2, tr=[0, lin(10), twohalfsin(5)], r=lin(x=360)),
//    actor("v", acts = [
//        act(d=2, tr=lin(x=5), r=lin(y=360), async=true),
//        act(s=1, d=2, v=lin(3)),
//    ]),
//];
//echo(s);
//
//echo(get_object_properties(s, 3.5, 0));

echo(get_object_properties(
[
    zeroact("c", tr=[0,0,0.5]),
    zeroact("v", v=0.1),
    act("c", d=2, tr=[0, lin(10), twohalfsin(5)], r=lin(x=360)),
    scene(d=2, acts = [
        act("c", tr=lin(x=5), r=lin(y=360)),
        act("v", s=1, v=lin(3)),
    ]),
    act("c", d=2, tr=twohalfsin(y=lin(-10), z=5), r=lin(z=360)),
    scene(d=2, acts = [
        act("c", tr=lin(x=-5), r=lin(y=-360)),
        act("v", v=lin(-3)),
    ]),
], 6, 0));
