include <animation.scad>
use <asserts.scad>
include <scenario_duration_tests.scad>

scenario = [];

assert_equal(is_applicable_to([], "a"), false);
assert_equal(is_applicable_to(["b"], "a"), false);
assert_equal(is_applicable_to(["a"], "a"), true);

assert_equal(has_element([1, 2, 3], 2), true);
assert_equal(has_element([1, 2, 3], 4), false);
assert_equal(has_element([1, 2, [3, 4]], 3), true);
assert_equal(has_element([1, 2, [3, 4]], 4), false);

assert_equal(is_empty(0), true);
assert_equal(is_empty(3), true);
assert_equal(is_empty(undef), true);
assert_equal(is_empty([]), true);
assert_equal(is_empty([0]), false);

//assert_equal(merge_act_properties([1, 2], [3, 4], 0), [4, 6]);
//assert_equal(merge_act_properties([1, 2], [3, 4], 1), [6]);
//assert_equal(merge_act_properties([1, [2, 3]], [4, [5, 6]], 0), [5, [7, 9]]);
//assert_equal(merge_act_properties([1, [2, 3]], [4, [5, 6]], 1), [[7, 9]]);
//assert_equal(merge_act_properties([], [4, [5, 6]], 0), [4, [5, 6]]);
//assert_equal(merge_act_properties([], [4, [5, 6]], 1), [[5, 6]]);
//assert_equal(merge_act_properties([], [], 0), []);

assert_equal(concat_as_is(1, 2), [1,2]);
assert_equal(concat_as_is([1], 2), [[1],2]);
assert_equal(concat_as_is(1, [2]), [1,[2]]);
assert_equal(concat_as_is([1], [2]), [[1],[2]]);
assert_equal(concat_as_is(1, undef), [1]);
assert_equal(concat_as_is([1], undef), [[1]]);
assert_equal(concat_as_is([], 2), [[],2]);
assert_equal(concat_as_is(1, []), [1,[]]);
assert_equal(concat_as_is([], []), [[],[]]);
assert_equal(concat_as_is([], undef), [[]]);

assert_equal(list_of_size_with([], 5, 0), []);
assert_equal(list_of_size_with([1], 5, 0), [5]);
assert_equal(list_of_size_with([1, 2], 5, 0), [5, 0]);
assert_equal(list_of_size_with([1, 2], 5, 1), [0, 5]);
assert_equal(list_of_size_with([1, 2, 3], 5, 0), [5, 0, 0]);
assert_equal(list_of_size_with([1, 2, 3], 5, 1), [0, 5, 0]);
assert_equal(list_of_size_with([1, 2, 3], 5, 2), [0, 0, 5]);

assert_equal(sum_elements(1, 2), 3);
assert_equal(sum_elements("1", "2"), "1");
assert_equal(sum_elements(true, false), true);
assert_equal(sum_elements(false, true), true);
assert_equal(sum_elements(true, true), true);
assert_equal(sum_elements(false, false), false);

assert_equal(sum_lists([], []), []);
assert_equal(sum_lists([1], [2]), [3]);
assert_equal(sum_lists([[1]], [[2]]), [[3]]);
assert_equal(sum_lists(["a", 1], ["b", 2]), ["a", 3]);
assert_equal(sum_lists(["a", 1, [3, 4, 5]], ["b", 2, [6, 7, 8]]), ["a", 3, [9, 11, 13]]);
assert_equal(sum_lists(["a", [3, 4, 5], 1], ["b", [6, 7, 8], 2]), ["a", [9, 11, 13], 3]);
assert_equal(sum_lists([3, 4, 5], [6, 7, 8]), [9, 11, 13]);

assert_equal(enrich_scenario([["a", 0, 1], ["a", 0, 1]]), [["a", 0, 1], ["a", 0, 1]]);
assert_equal(enrich_scenario([["a", 0, 7], ["a", undef, 12], ["a", undef, 1]]), [["a", 0, 7], ["a", 7, 12], ["a", 19, 1]]);
assert_equal(enrich_scenario([["a", undef, 7], ["a", undef, 12], ["a", undef, 1]]), [["a", 0, 7], ["a", 7, 12], ["a", 19, 1]]);
assert_equal(enrich_scenario([["a", 0, 7], ["a", undef, 12], ["a", 5, 17], ["a", undef, 1]]), [["a", 0, 7], ["a", 7, 12], ["a", 5, 17], ["a", 22, 1]]);
assert_equal(enrich_scenario([["c", 0, 1, ["lin", [0, 0, 9]], []], ["c", 1, 1, ["lin", [0, 0, -9]], []]]), [["c", 0, 1, ["lin", [0, 0, 9]], []], ["c", 1, 1, ["lin", [0, 0, -9]], []]]);
default_act_duration = 3;
assert_equal(enrich_scenario([["a", 0, 1], ["a", 0, 1]]), [["a", 0, 1], ["a", 0, 1]]);
assert_equal(enrich_scenario([["a", 0, 7], ["a", undef, undef], ["a", undef, undef]]), [["a", 0, 7], ["a", 7, 3], ["a", 10, 3]]);
assert_equal(enrich_scenario([["a", undef, 7], ["a", undef, 12], ["a", undef, undef]]), [["a", 0, 7], ["a", 7, 12], ["a", 19, 3]]);
assert_equal(enrich_scenario([["a", 0, 7], ["a", undef, undef], ["a", 5, undef], ["a", undef, 1]]), [["a", 0, 7], ["a", 7, 3], ["a", 5, 3], ["a", 8, 1]]);

assert_equal(sublist([], 0), []);
assert_equal(sublist([1], 0), [1]);
assert_equal(sublist([1], 1), []);
assert_equal(sublist([1,2], 0), [1, 2]);
assert_equal(sublist([1,2], 1), [2]);
assert_equal(sublist([1,2], 0, 1), [1]);
assert_equal(sublist([[3,4],2], 0), [[3,4],2]);
assert_equal(sublist([1,2,3], 0), [1,2,3]);
assert_equal(sublist([1,2,3], 1), [2,3]);
assert_equal(sublist([1,2,3], 2), [3]);
assert_equal(sublist([1,2,3], 3), []);
assert_equal(sublist([1,2,3], 0, 0), []);
assert_equal(sublist([1,2,3], 0, 1), [1]);
assert_equal(sublist([1,2,3], 0, 2), [1, 2]);
assert_equal(sublist([1,2,3], 0, 3), [1, 2, 3]);
assert_equal(sublist([1,2,3], 0, 4), [1, 2, 3]);
assert_equal(sublist([1,2,3], 1, 0), []);
assert_equal(sublist([1,2,3], 1, 1), [2]);
assert_equal(sublist([1,2,3], 1, 2), [2, 3]);
assert_equal(sublist([1,2,3], 1, 3), [2, 3]);
assert_equal(sublist([1,2,3], 2, 0), []);
assert_equal(sublist([1,2,3], 2, 1), [3]);
assert_equal(sublist([1,2,3], 2, 2), [3]);
assert_equal(sublist([1,2,3], 3, 1), []);

assert_equal(replace_element_with([1,2,3,4], 5, -1), [1,2,3,4]);
assert_equal(replace_element_with([1,2,3,4], 5, 0), [5,2,3,4]);
assert_equal(replace_element_with([1,2,3,4], 5, 1), [1,5,3,4]);
assert_equal(replace_element_with([1,2,3,4], 5, 2), [1,2,5,4]);
assert_equal(replace_element_with([1,2,3,4], 5, 3), [1,2,3,5]);
assert_equal(replace_element_with([1,2,3,4], 5, 4), [1,2,3,4]);
assert_equal(replace_element_with(["a", undef, 12], 7, 1), ["a", 7, 12]);

assert_equal(flatten_act_list([["a", 0, 1, [], []]]), [["a", 0, 1, [], []]]);
assert_equal(flatten_act_list([[["a", 0, 1, [], []]]]), [["a", 0, 1, [], []]]);
assert_equal(flatten_act_list([["a", 0, 1, [], []], ["a", 0, 2, [], []]]), [["a", 0, 1, [], []], ["a", 0, 2, [], []]]);
assert_equal(flatten_act_list([[["a", 0, 1, [], []], ["a", 0, 2, [], []]]]), [["a", 0, 1, [], []], ["a", 0, 2, [], []]]);
assert_equal(flatten_act_list([["a", 0, 1, [], []], [["a", 0, 2, [], []], ["a", 0, 3, [], []]]]), [["a", 0, 1, [], []], ["a", 0, 2, [], []], ["a", 0, 3, [], []]]);
assert_equal(flatten_act_list([[["a", 0, 1, [], []], ["a", 0, 2, [], []]], ["a", 0, 3, [], []]]), [["a", 0, 1, [], []], ["a", 0, 2, [], []], ["a", 0, 3, [], []]]);
assert_equal(flatten_act_list([[["a", 0, 1, [], []], ["a", 0, 2, [], []]], [["a", 0, 3, [], []], ["a", 0, 4, [], []]]]), [["a", 0, 1, [], []], ["a", 0, 2, [], []], ["a", 0, 3, [], []], ["a", 0, 4, [], []]]);

assert_equal(insert_element_at([], 11, 1), []);
assert_equal(insert_element_at([], 11, 0), [11]);
assert_equal(insert_element_at([1, 2], 11, 0), [11, 1, 2]);
assert_equal(insert_element_at([1, 2], 11, 1), [1, 11, 2]);
assert_equal(insert_element_at([1, 2], 11, 2), [1, 2, 11]);

assert_equal(actor("a", []), []);
assert_equal(actor("a", [[undef, 1]]), [["a", 1]]);
assert_equal(actor("a", [["b", 1]]), [["a", 1]]);
assert_equal(actor("a", [[undef, 1], [undef, 2]]), [["a", 1], ["a", 2]]);
assert_equal(actor("a", [["b", 1], ["c", 2]]), [["a", 1], ["a", 2]]);
assert_equal(actor("a", [[undef, 1], ["b", 2], [undef, 3]]), [["a", 1], ["a", 2], ["a", 3]]);
assert_equal(actor("a", [["b", 1], [undef, 2], ["c", 3]]), [["a", 1], ["a", 2], ["a", 3]]);

assert_equal(scene(acts=[["", 1, 2],["", 3, 4]]), [["", 1, 2],["", 3, 4]]);
assert_equal(scene(acts=[[["", 1, 2],["", 3, 4]]]), [["", 1, 2],["", 3, 4]]);
assert_equal(scene(s=5, acts=[["", 1, 2],["", 3, 4]]), [["", 6, 2],["", 8, 4]]);
assert_equal(scene(d=5, acts=[["", 1, 2],["", 3, undef]]), [["", 1, 2],["", 3, 5]]);
assert_equal(scene(s=6, d=5, acts=[["", 1, 2],["", 3, undef]]), [["", 7, 2],["", 9, 5]]);

assert_equal(get_current_act_value("linear", 0.5, [360, 0, 0]), [180, 0, 0]);
assert_equal(get_current_act_value("linear", 0.5, [360, ["halfsin", 360], 0]), [180, process_ratio("halfsin", 0.5)*360, 0]);
assert_equal(get_current_act_value("linear", 0.5, 5), 2.5);
assert_equal(get_current_act_value("linear", 0.5, [5]), [2.5]);

assert_equal(need_to_flatten([[]]), true);
assert_equal(need_to_flatten([[],[],[],[],[],[],[]]), true);
assert_equal(need_to_flatten(["s",1,2,[],[]]), false);
