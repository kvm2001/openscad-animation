include <animation.scad>
use <asserts.scad>

assert_equal(get_raw_act_durations(act(d=2)), [0, 2, 0]);
assert_equal(get_raw_act_durations(act(s=1, d=2)), [0, 3, 0]);
assert_equal(get_raw_act_durations(act(s=-1, d=2)), [1, 1, 0]);
assert_equal(get_raw_act_durations(act(d=2, async=true)), [0, 0, 2]);
assert_equal(get_raw_act_durations(act(s=1, d=2, async=true)), [0, 0, 3]);
assert_equal(get_raw_act_durations(act(s=-1, d=2, async=true)), [1, 0, 1]);
assert_equal(get_raw_act_durations(act(s=1), 2, 2, true), [0, 0, 1]);

assert_equal(get_actor_durations(actor("", [act(d=2), act(d=2)])), [0, 4, 0]);
assert_equal(get_actor_durations(actor("", [act(d=2), act(s=1, d=2)])), [0, 5, 0]);
assert_equal(get_actor_durations(actor("", [act(s=1, d=2), act(d=2)])), [0, 5, 0]);
assert_equal(get_actor_durations(actor("", [act(d=2), act(s=-1, d=2)])), [0, 3, 0]);
assert_equal(get_actor_durations(actor("", [act(s=-1, d=2), act(d=2)])), [1, 3, 0]);
assert_equal(get_actor_durations(actor("", [act(d=2), act(d=2, async=true)])), [0, 2, 2]);
assert_equal(get_actor_durations(actor("", [act(d=2, async=true), act(d=2)])), [0, 2, 0]);
assert_equal(get_actor_durations(actor("", [act(d=2), act(s=1, d=2, async=true)])), [0, 2, 3]);
assert_equal(get_actor_durations(actor("", [act(s=1, d=2, async=true), act(d=2)])), [0, 2, 1]);
assert_equal(get_actor_durations(actor("", [act(d=2), act(s=-1, d=2, async=true)])), [0, 2, 1]);
assert_equal(get_actor_durations(actor("", [act(d=2), act(s=-1, d=5, async=true)])), [0, 2, 4]);
assert_equal(get_actor_durations(actor("", [act(s=-1, d=2, async=true), act(d=2)])), [1, 2, 0]);
assert_equal(get_actor_durations(actor("", [act(s=1, d=2), act(s=1, d=2)])), [0, 6, 0]);
assert_equal(get_actor_durations(actor("", [act(s=1, d=2), act(s=-1, d=2)])), [0, 4, 0]);
assert_equal(get_actor_durations(actor("", [act(s=-1, d=2), act(s=1, d=2)])), [1, 4, 0]);
assert_equal(get_actor_durations(actor("", [act(s=1, d=2), act(d=2, async=true)])), [0, 3, 2]);
assert_equal(get_actor_durations(actor("", [act(d=2, async=true), act(s=1, d=2)])), [0, 3, 0]);
assert_equal(get_actor_durations(actor("", [act(s=1, d=2), act(s=1, d=2, async=true)])), [0, 3, 3]);
assert_equal(get_actor_durations(actor("", [act(s=1, d=2, async=true), act(s=1, d=2)])), [0, 3, 0]);
assert_equal(get_actor_durations(actor("", [act(s=1, d=2), act(s=-1, d=2, async=true)])), [0, 3, 1]);
assert_equal(get_actor_durations(actor("", [act(s=-1, d=2, async=true), act(s=1, d=2)])), [1, 3, 0]);
assert_equal(get_actor_durations(actor("", [act(s=-1, d=2), act(s=-1, d=2)])), [1, 2, 0]);
assert_equal(get_actor_durations(actor("", [act(s=-1, d=2), act(d=2, async=true)])), [1, 1, 2]);
assert_equal(get_actor_durations(actor("", [act(d=2, async=true), act(s=-1, d=2)])), [1, 1, 1]);
assert_equal(get_actor_durations(actor("", [act(s=-1, d=2), act(s=1, d=2, async=true)])), [1, 1, 3]);
assert_equal(get_actor_durations(actor("", [act(s=1, d=2, async=true), act(s=-1, d=2)])), [1, 1, 2]);
assert_equal(get_actor_durations(actor("", [act(s=-1, d=2), act(s=-1, d=2, async=true)])), [1, 1, 1]);
assert_equal(get_actor_durations(actor("", [act(s=-1, d=2), act(s=-1, d=5, async=true)])), [1, 1, 4]);
assert_equal(get_actor_durations(actor("", [act(s=-1, d=2, async=true), act(s=-1, d=2)])), [1, 1, 0]);
assert_equal(get_actor_durations(actor("", [act(s=-1, d=5, async=true), act(s=-1, d=2)])), [1, 1, 3]);
assert_equal(get_actor_durations(actor("", [act(d=2, async=true), act(d=2, async=true)])), [0, 0, 2]);
assert_equal(get_actor_durations(actor("", [act(d=2, async=true), act(s=1, d=2, async=true)])), [0, 0, 3]);
assert_equal(get_actor_durations(actor("", [act(s=1, d=2, async=true), act(d=2, async=true)])), [0, 0, 3]);
assert_equal(get_actor_durations(actor("", [act(d=2, async=true), act(s=-1, d=2, async=true)])), [1, 0, 2]);
assert_equal(get_actor_durations(actor("", [act(s=-1, d=2, async=true), act(d=2, async=true)])), [1, 0, 2]);
assert_equal(get_actor_durations(actor("", [act(s=1, d=2, async=true), act(s=1, d=2, async=true)])), [0, 0, 3]);
assert_equal(get_actor_durations(actor("", [act(s=1, d=2, async=true), act(s=-1, d=2, async=true)])), [1, 0, 3]);
assert_equal(get_actor_durations(actor("", [act(s=-1, d=2, async=true), act(s=1, d=2, async=true)])), [1, 0, 3]);
assert_equal(get_actor_durations(actor("", [act(s=-1, d=2, async=true), act(s=-1, d=2, async=true)])), [1, 0, 1]);
assert_equal(get_actor_durations(actor("", [act(s=-4, d=2, async=true), act(s=-1, d=2, async=true)])), [4, 0, 1]);
assert_equal(get_actor_durations(actor("", [act(s=-1, d=2, async=true), act(s=-4, d=2, async=true)])), [4, 0, 1]);
assert_equal(get_actor_durations(actor("", [zeroact(), act(d=2)])), [0, 2, 0]);
assert_equal(get_actor_durations(actor("", [zeroact(), act(s=1, d=2)])), [0, 3, 0]);
assert_equal(get_actor_durations(actor("", [zeroact(), act(s=-1, d=2)])), [1, 1, 0]);
assert_equal(get_actor_durations(actor("", [zeroact(), act(d=2, async=true)])), [0, 0, 2]);
assert_equal(get_actor_durations(actor("", [zeroact(), act(s=1, d=2, async=true)])), [0, 0, 3]);
assert_equal(get_actor_durations(actor("", [zeroact(), act(s=-1, d=2, async=true)])), [1, 0, 1]);
assert_equal(get_actor_durations(actor("", [act(d=2), zeroact()])), [0, 2, 0]);
assert_equal(get_actor_durations(actor("", [act(s=1, d=2), zeroact()])), [0, 3, 0]);
assert_equal(get_actor_durations(actor("", [act(s=-1, d=2), zeroact()])), [1, 1, 0]);
assert_equal(get_actor_durations(actor("", [act(d=2, async=true), zeroact()])), [0, 0, 2]);
assert_equal(get_actor_durations(actor("", [act(s=1, d=2, async=true), zeroact()])), [0, 0, 3]);
assert_equal(get_actor_durations(actor("", [act(s=-1, d=2, async=true), zeroact()])), [1, 0, 1]);

assert_equal(get_scene_durations(scene(0, 2,[act(), act()])), [0, 2, 0]);
assert_equal(get_scene_durations(scene(1, 2,[act(), act()])), [0, 3, 0]);
assert_equal(get_scene_durations(scene(-1, 2,[act(), act()])), [1, 1, 0]);
assert_equal(get_scene_durations(scene(0, 2,[act(d=1), act()])), [0, 2, 0]);
assert_equal(get_scene_durations(scene(0, 2,[act(), act(d=1)])), [0, 2, 0]);
assert_equal(get_scene_durations(scene(0, 2,[act(d=3), act()])), [0, 2, 0]);
assert_equal(get_scene_durations(scene(0, 2,[act(), act(d=3)])), [0, 2, 0]);
assert_equal(get_scene_durations(scene(0, 2,[act(d=3), act(d=4)])), [0, 2, 0]);
assert_equal(get_scene_durations(scene(0, 2,[act(d=4), act(d=3)])), [0, 2, 0]);
assert_equal(get_scene_durations(scene(0, 2,[act(s=1), act()])), [0, 2, 0]);
assert_equal(get_scene_durations(scene(0, 2,[act(), act(s=1)])), [0, 2, 0]);
assert_equal(get_scene_durations(scene(0, 2,[act(s=-1), act()])), [1, 2, 0]);
assert_equal(get_scene_durations(scene(0, 2,[act(), act(s=-1)])), [1, 2, 0]);
assert_equal(get_scene_durations(scene(-2, 2,[act(s=-1), act()])), [2, 0, 0]);
assert_equal(get_scene_durations(scene(-2, 2,[act(), act(s=-1)])), [2, 0, 0]);
assert_equal(get_scene_durations(scene(-1, 2,[act(s=-2), act()])), [2, 1, 0]);
assert_equal(get_scene_durations(scene(-1, 2,[act(), act(s=-2)])), [2, 1, 0]);

assert_equal(merge_durations([0, 0, 0], [1, 7, 1]), [1, 7, 1]);
assert_equal(merge_durations([0, 0, 0], [1, 6, 0]), [1, 6, 0]);

assert_equal(get_acts_durations([
    zeroact("c", tr=[0,0,0.5]),
    zeroact("v", v=0.1),
    act("c", d=2, tr=[0, lin(10), twohalfsin(5)], r=lin(x=360)),
    scene(d=2, acts = [
        act("c", tr=lin(x=5), r=lin(y=360)),
        act("v", v=lin(3)),
    ]),
    act("c", d=2, tr=twohalfsin(y=lin(-10), z=5), r=lin(z=360)),
    scene(d=2, acts = [
        act("c", d=5, tr=lin(x=-5), r=lin(y=-360)),
        act("c", tr=lin(x=-5), r=lin(y=-360)),
        act("v", d=1, v=lin(-3)),
    ]),
]), [0, 8, 0]);

assert_equal(get_acts_durations([
    zeroact("c", tr=[0,0,0.5]),
    zeroact("v", v=0.1),
    act("c", d=2, tr=[0, lin(10), twohalfsin(5)], r=lin(x=360)),
    scene(d=2, acts = [
        act("c", tr=lin(x=5), r=lin(y=360)),
        act("v", v=lin(3)),
    ]),
    act("c", d=2, tr=twohalfsin(y=lin(-10), z=5), r=lin(z=360)),
    scene(d=2, acts = [
        act("c", tr=lin(x=-5), r=lin(y=-360)),
        act("v", d=1, v=lin(-3)),
    ]),
]), [0, 8, 0]);

assert_equal(get_acts_durations([
    zeroact("c", tr=[0,0,0.5]),
    zeroact("v", v=0.1),
    act("c", d=2, tr=[0, lin(10), twohalfsin(5)], r=lin(x=360)),
    scene(d=2, acts = [
        act("c", tr=lin(x=5), r=lin(y=360)),
        act("v", v=lin(3)),
    ]),
    act("c", d=2, tr=twohalfsin(y=lin(-10), z=5), r=lin(z=360)),
    scene(d=2, acts = [
        act("c", tr=lin(x=-5), r=lin(y=-360)),
        act("v", v=lin(-3)),
    ]),
]), [0, 8, 0]);

assert_equal(get_acts_durations([
    zeroact("c", tr=[0,0,0.5]),
    zeroact("v", v=0.1),
    act("c", s=-1, d=2, tr=[0, lin(10), twohalfsin(5)], r=lin(x=360)),
    scene(d=2, acts = [
        act("c", tr=lin(x=5), r=lin(y=360)),
        act("v", s=1, v=lin(3)),
    ]),
    act("c", d=2, tr=twohalfsin(y=lin(-10), z=5), r=lin(z=360)),
    scene(d=2, acts = [
        act("c", tr=lin(x=-5), r=lin(y=-360)),
        act("v", d=1, v=lin(-3)),   
    ]),
]), [1, 7, 0]);

assert_equal(get_acts_durations([
    zeroact("c", tr=[0,0,0.5]),
    zeroact("v", v=0.1),
    act("c", d=2, tr=[0, lin(10), twohalfsin(5)], r=lin(x=360)),
    scene(d=2, acts = [
        act("c", tr=lin(x=5), r=lin(y=360)),
        act("v", s=-5, v=lin(3)),
    ]),
    act("c", d=2, tr=twohalfsin(y=lin(-10), z=5), r=lin(z=360)),
    scene(d=2, acts = [
        act("c", tr=lin(x=-5), r=lin(y=-360)),
        act("v", d=1, v=lin(-3)),
    ]),
]), [3, 8, 0]);
