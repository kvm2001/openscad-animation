# Concepts

## Scenario
Eventually *scenario* consists of *acts* or lists of acts (scenes and actors). List of acts can contain another list of acts recursively with no limitations enclosing.

    scenario = [act, act, [act, [act, [...]]], ...]

## Absolute and relative values
Values of actor properties used in acts can be *absolute* or *relative*.

* When *relative* values are used, then transformations applied in the current act are started from the last object state from the end of the previous act. In other words, properties from all acts from start till current act are summed.
* When *absolute* values are used, then they are set from initial actor state.

## Act
*Act* is a list of actor properties for a time interval. Acts follow one by one. By default next act starts immediately when previous act finishes.

    act = [actor_name, act_start_time, duration, translate, rotate, value, async]

where:

* *actor_name* of that act
* *act_start_time* is time when act starts (in seconds) relative to the previous act end. 
    * If it equals to zero, then act starts right after previous act (0 value can be omitted). 
    * If it is positive then there will be a pause between previous act and current act.
    * If it is negative then current act will start before the previous one is finished (earlier on the given amount of seconds).
* *duration* of the act in seconds
* *translate* of the actor. Applied when actor is an object.
* *rotate* of the actor. Applied when actor is an object.
* *value* of the actor. Applied when actor is a value (number).
* *async* of the act. False by default.
    * If it is *true* then next act will start immediately with current one (like if current act had no duration). Current act still can have duration set. The only difference that current act and next act will be executed in parallel.
    * If it is *false* (by default) then next act will start only when current one ends.

By default values set in *translate*, *rotate* and *value* are *relative*.

### Zero act
Is an act with zero duration. `zeroact` dsl function can be used for this.

    zeroact = [actor_name, act_start_time, translate, rotate, value]

Since zero acts have no duration `async` property has no sense, so not used.

This is useful for initial properties set up for an actor. Can be used not only at the start of the scenario but in the middle as well. By default values set in zero acts are *absolute*.
## Scene
*Scene* is a list of async acts. Scene itself can have some duration and start time as well.

    scene = [scene_start_time, duration, [act, act, [act, [...]], ...]]

There is no need to set `async=true` for every act inside of scene. `scene` dsl function will do this.

Intention of the `scene` dsl function is not to repeat duration for all acts which should go in parallel.

Acts inside of the scene can override start time and duration. In this case start time is measured relative to the scene start time.

## Actor
There are two kinds of actors:

* Object (like result of calling some `module`)
* Value (like result of calling some `function`)

And `actor` is another dsl function. It is helpful when there is a sequence of act of a single actor. In this case you can mention actor only once and this function will set the actor for every act inside.

    actor = [actor_name, [act, act, [act, [...]], ...]]

## Interpolation functions
To be added

# Processed scenario (internal representation)
*Processed scenario* is a list of actor properties.

    processed_scenario = [
        [actor_name, translate, rotate, value]],
        ...
    ]

There is no start_time and duration values since they were already used to get right values in object properties (tr, r, v). So we don't need to store them in the scenario.

This way it is even easier to get object data on demand. We need no calculation at this point.

Processed scenario contains properties only from acts passed or in progress. Future acts are not counted.

# Scenario transformation

Scenario duration and current time in seconds should be calculated prior to scenario processing. This is needed to understand whether act passed, in progress or not started yet. And for acts in progress it is needed to calculate interpolation function if any is used.

When an act is processed then

1. If act hasn't started yet, then it is skipped
1. If act has passed completely then its final values are used (not always the same as passed in act literally)
1. If act is in progress then interpolated values are calculated using act ratio (how much of act already have passed)
1. Then this value is merged (absolute or relative) with the last calculated object state and added to the state list as a first element (to ease looking for the last calculated state)

After we get it, we need to iterate over all calculated value list and choose only last calculated value for each actor in the list.

**(Implemented!)** Another point is that we have to know total duration before such processing. It is needed for converting `$t` to `current_time` in seconds from scenario start. We need `current_time` to understand whether act is passed, active, or from future. So, duration calculation should be done prior to scenario processing.

**(Implemented!)** When scenario is processed a lot of work should be done on arrays of values. So, we should have pretty many common list operations, like adding a value at an index, replacing at index, merging list values (like summing), etc.

## Value processing

There are some values with strongly defined data types like, start_time and duration are a single number, translation and rotation are a `[x, y, z]` vector. But there could be unknown values with undefined (at code implementation time) types.

So, it is sound reasonable, that the code (for example, summing of properties during acts processing) should be ready to process any type of values, but it should have some faster implementations for strongly defined values (like `[x, y, z]`, etc.).

# Duration calculation **(Implemented!)** 

It is not straightforward because of irregular structure of the initial (raw) scenario. 

* Scenario (first item of scenario) starts at 0 seconds if other value isn't set.
* Subsequent items start at the previous item end if other isn't specified.
* If previous item of scenario is set as `async=true`, then item after this async item starts at async item start (not at its end).
* In theory it is possible for scenario items to have a negative value at start (absolute or relative?). So, it is possible that some items could start at negative time in raw scenario. I haven't decided yet what to do with them. I see two options:
    * Truncate these acts (skip negative timings of the scenario)
    * Extend scenario duration to the negative part
* Start_time of acts which are inside of scenes should be calculated relative to the scene's start_time.
* Scene duration is a duration of the longest act inside of scene.

Scenario duration is a max value of start_time + duration across all acts.

When calculating scenario duration, then these values should be passed using parameters of recursive function:

* max_known_time
* prev_item_start (used for async items)
* prev_item_duration
* scenario_item_index
* enclosed_item_index (used for lists of acts inside of scenario)

When calculating scenario duration no actors and scenes should present in scenario because they're processed before this point of time (as DSL functions). So scenario at this time should be a list of acts and lists of acts. And there could be lists of lists of acts if an actor was a part of a scene, or if a scene was a part of other scene. But it is better to avoid such a structure to more plain one. So, these DSL functions have to return plain lists of acts.

Final decision is to have such kind of scenario structure when duration is calculated:

    scenario = [
        act,
        [ act, act]
        act,
        ...
    ]

# Object kind detection

There are several different kinds of special objects and values:

* Scenario
* Scene
* Actor (list of acts of the same actor)
* Act
* Interpolation functions (lin, sinus, halfsin, etc.)

To distinguish between them it was decided to follow the approach used for interpolation functions: a list is created wrapping the object name and its value. Some examples:

* `["act", ["cube", 3, 5, [0, 0, 0], ...]]`
* `["scene", [act1, act2, ...]]`
* `["linear", [1, 2, 3]]`
* `["sinus", 5]`

It could be an exception for scenario, because it is always a root object and has only one appearance (scenario must not contain other scenarios).

As an option, to avoid these names for acts, actors and scenes it is possible to allow these DSL functions to return a list of enriched acts (already with needed values set, like start_time, actor_name, etc.).

So, all DSL helper functions should return objects they generate with all needed (known) data already set to them.

DSL functions which can group acts should return a plain list of acts even if they where enclosed to each other. This helps with scenario duration calculation and with iterating over the scenario in general.
